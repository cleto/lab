package main

import (
	"fmt"
	"math"
	"math/rand"
)

// add is a simple function for adding two numbers
// func add(x int, y int) int {
func add(x, y int) int {
	return x + y
}

func say_hello(x string) (string, string) {
	return "Hello", x
}

func half(x int) (result float64) {
	result = float64(x) / 2.0
	return
}

func local_variable() {
	var var1 int
	fmt.PrintLn(var1)
	return
}

var global int

func main() {
	fmt.Println("My favorite number is", rand.Intn(10))
	fmt.Println(
		"Names are exported if they are capitalized, like math.Pi:",
		math.Pi)
	fmt.Println("Add 43 + 28:", add(43, 28))
	first, second := say_hello("John")
	fmt.Println("say_hello:", first, second)
	fmt.Println("Half of 15:", half(15))
	fmt.Println("Global:", global)
	local_variable()
}
