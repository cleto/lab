function print_types() {
    function create_header2(title) {
	var hdr = document.createElement("h2");
	var text = document.createTextNode(title);
	hdr.appendChild(text);
	document.body.appendChild(hdr);
    }

    function create_paragraph(input_text) {
	var par = document.createElement("p");
	var text = document.createTextNode(input_text);
	par.appendChild(text);
	document.body.appendChild(par);
    }

    create_header2("Basic types");
    create_paragraph(1 + " is a " + typeof(1));
    create_paragraph(1.5 + " is a " + typeof(1.5));
    create_paragraph(true + " is a " + typeof(true));
    create_paragraph("'Hello'" + " is a " + typeof("Hello"));
    create_paragraph(null + " is a " + typeof(null));



    create_header2("Using variables");

    var number = 1;
    create_paragraph(number + " is a " + typeof(number));
    number = 1.5;
    create_paragraph(number + " is a " + typeof(number));

    var bool = true;
    create_paragraph(bool + " is a " + typeof(bool));

    var str = "'Hello'";
    create_paragraph(str + " is a " + typeof(str));

    var nil = null;
    create_paragraph(nil + " is a " + typeof(nil));



    create_header2("Arrays");

    // Literal initialization
    var array1 = ["'Hello'", 1, 1.5, true, null];
    // This way of looping should be avoid
    for (var i in array1) {
	create_paragraph("Array1: " + array1[i] + " is a " + typeof(array1[i]));
    }

    // Condensed form
    var array2 = new Array('Message', 1, 1.5, true, null);
    // This way of looping is more recommended
    for (var i=0; i<array2.length; i++) {
	create_paragraph("Array2: " + array2[i] + " is a " + typeof(array2[i]));
    }

    create_header2("Dictionaries");

    // This is an Object with 1,2,3... attributes
    var days = {
	1:'Monday',
	2:'Tuesday',
	3:'Wednesday',
	4:'Thursday',
	5:'Friday',
	6:'Saturday',
	7:'Sunday'
    };

    for (var i=0; i<days.length; i++) {
	create_paragraph(days[day] + ":" + days[day] + " = "
			 + typeof(day) + ":" + typeof(days[day]));
    }


}

print_types();
