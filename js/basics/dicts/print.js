function print_dict() {
    function create_paragraph(input_text) {
	var par = document.createElement("p");
	var text = document.createTextNode(input_text)
	par.appendChild(text);
	document.body.appendChild(par);
    }

    var days = {
	'Monday': 1,
	'Tuesday': 2,
	'Wednesday': 3,
	'Thursday': 4,
	'Friday': 5,
	'Saturday': 6,
	'Sunday': 7
    };

    for(var day in days) {
	create_paragraph(day + " is the " + days[day] + " day of the week");
    }
}

print_dict();
